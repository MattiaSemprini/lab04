package it.unibo.oop.lab04.bank2;

import it.unibo.oop.lab04.bank.*;

public abstract class AbstractBankAcoount implements BankAccount{

	   

		private double balance;
	    private int nTransactions;
	    private final int usrID;
	
	    public AbstractBankAcoount(double balance, int usrID) {
			super();
			this.balance = balance;
			this.nTransactions = 0;
			this.usrID = usrID;
		}
	    
	protected boolean checkUser(final int id) {
        return this.usrID == id;
    }
	
	@Override
	public void computeManagementFees(int usrID) {
		
		if (checkUser(usrID)) {
            this.balance -= SimpleBankAccount.MANAGEMENT_FEE;
        }

	}

	@Override
	public void deposit(int usrID, double amount) {
		this.transactionOp(usrID, amount);

	}

	@Override
	public void depositFromATM(int usrID, double amount) {
		this.deposit(usrID, amount - SimpleBankAccount.ATM_TRANSACTION_FEE);

	}
	
    protected void incTransactions() {
        this.nTransactions++;
    }

	@Override
	public double getBalance() {
		return this.balance;
	}

	@Override
	public int getNTransactions() {
		return this.nTransactions;
	}
	
	private void transactionOp(final int usrID, final double amount) {
        if (checkUser(usrID)) {
            this.balance += amount;
            this.incTransactions();
        }
    }

	@Override
	public void withdraw(int usrID, double amount) {
		this.transactionOp(usrID, -amount);

	}

	@Override
	public void withdrawFromATM(int usrID, double amount) {
		this.withdraw(usrID, amount + SimpleBankAccount.ATM_TRANSACTION_FEE);

		
	}
	
	protected abstract boolean isWithdrawAllowed(double allowed);
	
	protected abstract double computeFee();

}
	

